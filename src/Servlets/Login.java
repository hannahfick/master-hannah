package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        //todo add function to take a number, pass to a dao to retrieve the user info. dao should return success or failure, success is a user object, failure should trigger sending back to index/login page with a message that no user exists.

        HttpSession session = request.getSession(); // this will create a session if one doesn't exist.

        //connect to db and retriev user data
        UserDAO dbConection = new UserDAO(getServletContext());
        User user = dbConection.getUser(request.getParameter("id"));


    }


    //not used, will deal with requests via post to ensure confidentiality of log in numbers
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
}
